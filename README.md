# conference-webapp
You need to run `conference-gateway` application as backend module or to run both modules via Docker (check configuration below)

## IDEA configuration
Install plugin "Vue.js" to your editor

### Compiles and hot-reloads for development
```
npm run serve
```

### Docker configuration
* For creating an image run: `docker build -t conference-web .`
* Then run the container from the image: `docker run -it -p 8080:8080 --rm --name conference-web conference-web`
* Open in your browser `localhost:8080` to see User Interface

### Lints and fixes files
```
npm run lint
```


## Development notes

For modifying vue project parameters install Vue CLI

```sh
npm install -g @vue/cli
npm install @vue/cli-service -g

```

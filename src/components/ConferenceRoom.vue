<template>
  <b-card-body>
    <b-card-title id="room">Conference Room</b-card-title>
    <br>
    <b-card-text id="roomMissing" v-if="!room">No booked Room at the moment</b-card-text>
    <br>
    <b-form v-if="room" id="roomForm" ref="form">
      <b-form-group
          id="room-name-grp"
          v-model="room.roomName">
        <template >Room name: {{ room.roomName }}</template>
      </b-form-group>

      <b-form-group
          id="location-grp"
          v-model="room.location">
        <template >Location: {{ room.location }}</template>
      </b-form-group>

      <b-form-group
          id="room-size-grp"
          v-model="room.roomSize">
        <template >Room size: {{ room.roomSize }}</template>
      </b-form-group>
    </b-form>

    <b-form-group>
      <b-button variant="info"
                @click="getConferenceRooms"
                v-model="rooms">
        <b-icon-plus></b-icon-plus>
        <label>
          <b-form-row v-if="room">Change room</b-form-row>
          <b-form-row v-else>Book room to Conference</b-form-row>
        </label>
      </b-button>

      <b-row class="moreRooms">
        <div v-for="room in rooms" :key="room.id">
          <b-link
              v-on:mouseenter="hover = true"
              v-on:mouseleave="hover = false"
              @click="addRoomToConference(room)">
            {{ 'Name: ' + room.roomName + ' / Location: ' + room.location + ' / Size: ' + room.roomSize }}
          </b-link><br>
        </div>
      </b-row>
    </b-form-group>

  </b-card-body>
</template>

<script>
import EventBus from "@/utils/eventBus";

export default {
  name: "ConferenceRoom",
  props: {
    room: {
      type: Object,
      required: false
    },
    registeredParticipants: {
      type: Number,
      required: false
    },
    conferenceDate: {
      type: String,
      required: false
    }
  },
  data() {
    return {
      rooms: []
    }
  },
  methods: {
    async getConferenceRooms() {
      try {
        const response = await this.axios.post('/room/',
            {registeredParticipants: this.registeredParticipants,
              conferenceDate: this.conferenceDate }
        );
        if (response.data) {
          this.rooms = response.data;
          return this.rooms;
        }
        return this.rooms;
      }
      catch (e) {
        console.log(e);
      }
    },
    async addRoomToConference(room) {
      try {
        const response = await this.axios.patch(`/conference/${this.$route.params.id}/room`, room);
        if (response.data) {
          EventBus.$emit("FETCH_CONFERENCE");
        }
        return this.rooms = null;
      } catch (e) {
        console.log(e);
      }
    }
  }
}
</script>

<style lang="less">
#roomMissing {
  font-style: italic;
}

.moreRooms {
  padding-top: 20px;
}
</style>

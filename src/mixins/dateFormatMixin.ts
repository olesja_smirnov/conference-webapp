import moment from "moment";

export const dateFormatMixin = {
    methods: {
        dateTimeFormat (date: string | number | Date) {
            const d = new Date(date);
            const dm = moment(d);
            return dm.format('DD/MM/YYYY HH:mm');
        },
        dateFormat (date: string | number | Date) {
            const d = new Date(date);
            const dm = moment(d);
            return dm.format('DD/MM/YYYY');
        }
    }
}

// export default dateFormatMixin

export const highlightStyleMixin = {
    methods: {
        missingValueBorderHighlight(value: string) {
            if (!value) return 'is-invalid'
        },
        missingValueTextHighlight(value: string) {
            if (!value) return 'text-danger'
        },
    }
}
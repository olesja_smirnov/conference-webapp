import Vue from 'vue'
import VueRouter from 'vue-router'

import Conference from '../views/Conference.vue'
import HomePage from '../views/HomePage.vue'


Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'HomePage',
    component: HomePage,
    meta: {
      anon: true
    }
  },
  {
    path: '/conference/:id',
    name: 'Conference',
    component: Conference,
    meta: {
      anon: true
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router